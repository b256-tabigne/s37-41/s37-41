const Course = require("../models/Course.js");

// Create a new course
/*
	Business Logic:
	1. Create a new Course object using the mongoose model and the information from the request body and the id from the header
	2. Save the new Course to the database
*/
module.exports.addCourse = (course) => {

	// Creates a variable "newCourse" and instantiates a new "Course" object using the mongoose model
	// Uses the information from the request body to provide all the necessary information
	let newCourse = new Course({
		name : course.name,
		description : course.description,
		price : course.price
	});

	// Saves the created object to our database
	return newCourse.save().then((result, err) => {

		// Course creation successful
		if(err) {

			return false;

		// Course creation failed
		} else {

			return true;

		};
	});
};

// Retrieveing all courses
/*
	Business Logic:
	1. Retrieve all the courses from the database
*/
module.exports.getAllCourses = () => {

	return Course.find({}).then(result => {

		return result;

	});
};

// Retrieving All Active Courses
/*
	Business Logic:
	1. Retrieve all the courses from the database with the property of "isActive" to true
*/
module.exports.getActiveCourses = () => {

	return Course.find({isActive: true}).then(result => {

		return result;

	})
}

// Retrieving a Specific Course
/*
	Business Logic:
	1. Retrieve the course that matches the course ID provided from the URL
*/
// reqParams = req.params(url) = "/:courseId"
module.exports.getCourse = (reqParams) => {

	return Course.findById(reqParams.courseId).then(result => {

		return result;

	})
}

// Updating a Course
/*
	Business Logic:
	1. Create a variable "updatedCourse" which will contain the information retrieved from the request body
	2. Find and update the course using the course ID retrieved from the request params property and the variable "updatedCourse" containing the information from the request body
*/
// course = data.course = req.body
// paramsId = data.params = req.params = /:courseId
module.exports.updateCourse = (course, paramsId) => {

	// specify the field/properties that needs to be updated
	let updatedCourse = {
		name : course.name,
		description : course.description,
		price : course.price
	}

	// findByIdAndUpdate(id, updatedContent)
	return Course.findByIdAndUpdate(paramsId.courseId, updatedCourse).then((result, err) => {

		if(err) {

			return false;

		} else {

			return true;

		}
	})
}


module.exports.obtainCourse = (course, paramsId) => {

	let obtainCourse = {
		isActive: Boolean
	}

	return Course.findByIdAndUpdate(paramsId.courseId, obtainCourse).then((result, err) => {

		if(err) {

			return false;

		} else {

			return true;

		}
	})
}